export interface ICourse {
  user?: string;
  employNumber?: number;
  date?: Date;
  course?: string;
  hour?: string
}