export interface ICalendar {
    date: moment.Moment;
    enable: boolean;
}