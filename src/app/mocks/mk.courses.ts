import { ICourse } from '../interfaces/i.course';

export const LIST_COURSE: ICourse[] = [
  {
    user: 'Reinner Daza',
    employNumber: 165454,
    course: 'Curso 1',
    date: new Date(),
    hour: '11'
  },
  {
    user: 'Yosting Toston',
    employNumber: 194712,
    course: 'Curso 1',
    date: new Date(),
    hour: '10'
  },
  {
    user: 'Andrew Fon',
    employNumber: 178221,
    course: 'Curso 1',
    date: new Date(),
    hour: '16'
  },
]