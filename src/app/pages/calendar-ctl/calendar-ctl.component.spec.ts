import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarCTLComponent } from './calendar-ctl.component';

describe('CalendarCTLComponent', () => {
  let component: CalendarCTLComponent;
  let fixture: ComponentFixture<CalendarCTLComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarCTLComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarCTLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
