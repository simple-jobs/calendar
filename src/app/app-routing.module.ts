import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarCTLComponent } from './pages/calendar-ctl/calendar-ctl.component';


const routes: Routes = [
  { path: 'calendar', component: CalendarCTLComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }