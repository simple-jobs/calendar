import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ICalendar } from 'src/app/interfaces/i.calendar';
import { LIST_COURSE } from 'src/app/mocks/mk.courses';
import { ICourse } from 'src/app/interfaces/i.course';
import { LIST_NAME_DAYS } from 'src/app/models/m.calendar';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  public dynamicView = false;
  public listDayes = LIST_NAME_DAYS;
  public listDaysInMonth: ICalendar[] = [];
  public listTime = [];


  public currentStartDay = 0;
  public currentMonthIndex = 0;
  public currentStartDayNumber = 0;


  constructor() {

  }

  ngOnInit(): void {
    this.loadCalendar();
  }

  public getFormateDate(date: moment.Moment): any {
    return date.date();
  }

  public loadCalendar() {
    this.listDaysInMonth = [];
    this.currentStartDay = this.month.date();
    this.currentStartDayNumber = this.month.day();
    let diff = this.currentStartDayNumber - 1;
    for (let diffIndex = 0; diffIndex < diff; diffIndex++) {
      const beforeMonth = this.month.subtract(1, "month");
      const beforeDate = moment({ year: beforeMonth.year(), month: beforeMonth.month(), day: beforeMonth.daysInMonth() - diffIndex })
      this.listDaysInMonth.push({
        enable: moment() > beforeDate,
        date: beforeDate
      });
    }

    let daysInMonth = this.month.daysInMonth();
    for (let i = 1; i <= daysInMonth; i++) {
      let newMonth = moment({ year: this.month.year(), month: this.month.month(), day: i })
      this.listDaysInMonth.push({
        enable: newMonth > moment(),
        date: newMonth
      });
    }
  }

  public nextMonth() {
    this.currentMonthIndex--;
    this.loadCalendar();
  }

  public previousMonth() {
    this.currentMonthIndex++;
    this.loadCalendar();
  }

  public get month() {
    return moment().subtract(this.currentMonthIndex, "month").startOf('month');
  }

  public selectDate() {
    this.dynamicView = !this.dynamicView;
    this.createTimeList();
    if (this.dynamicView)
      this.listDaysInMonth = this.listDaysInMonth.filter((element) => element.enable);
    else
      this.loadCalendar()
  }

  public createTimeList() {
    this.listTime = [];
    for (let index = 6; index <= 20; index++) {
      const list = this.getUserTimeCourse(`${index}`);
      if (list.length > 0) {
        const item = list[0];
        item.hour = this.getHour(index);
        this.listTime.push(item);
      } else {
        const item: ICourse = {
          hour: this.getHour(index)
        }
        this.listTime.push(item);
      }
    }
  }

  public getHour(hour: number): string {
    if (hour <= 12) {
      if (hour < 10) return '0' + hour + ':00 am';
      else return hour + ':00 am';
    } else {
      const finalHour = hour - 12;
      if (finalHour < 10) return '0' + finalHour + ':00 pm';
      else return finalHour + ':00 pm';
    }
  }

  public getUserTimeCourse(hour: string) {
    return LIST_COURSE.filter((user) => user.hour == hour);
  }
}
